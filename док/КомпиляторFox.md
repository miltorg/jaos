# Компилятор Fox

### Модуль фронтенда

```
Compiler.Mod /GetOptions
Frontend.Mod /GetFrontendByName ("Oberon")
FoxOberonFrontend.Mod /Frontend.Initialize
Parse

```

### Что-то про символьные файлы и порядок инициализации


Compiler.Modules 
FoxBackend.Initialize  
(* Неясно с чего, но мы верим, что используется FoxTextualSymbolFile, впрочем, нам не это важно *)  
FoxTextualSymbolFile.Initialize (?)  
FoxFormats (* могут иметь отношение к симв. файлу *)  

(** generate symbol file **)  
IF (options.symbolFile # NIL) & ~options.symbolFile.Export(module, importCache) =>   
(* Как-то так *)   
FoxTextualSymbolFile.TextualSymbolFile.Export(module:SyntaxTree.Module; importCache: SyntaxTree.ModuleScope)  
 Ищем ModuleScope (хотим в него подсунуть константу) - он растёт от просто FoxSyntaxTree.Scope (искать <<Scope=*OBJECT>>)  
   PROCEDURE AddConstant - добавляем константу (типа Constant)

