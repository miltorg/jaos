��������� ������ PodrobnajaPechatq ���������, �����
===================================================

������ ���������� ��� ������ Module.internal . 

� ��������� ��������: 
```
(* internal pointer array: to protect internal data structures from being GCed *)
� �����, 
	(* the correponding name array is protected from being GCed via module's internal pointer arrray 
		compiler generated!
	*)
DynamicName* = POINTER {UNSAFE} TO ARRAY 256 OF CHAR;
```

���� "internal" � Fox*.Mod, ������� � FoxIntermediateBackend: 
```
			Info(moduleSection,"internal: POINTER TO ARRAY OF Pointer");
			Symbol(moduleSection, modulePointerSection, modulePointerSectionOffset, 0);
```

���� modulePointerSectionOffset - ������ ���������. 
```
				(*! require GC protection *)
				modulePointerSection := Block("Heaps","ArrayBlockDesc",".@ModulePointerArray", modulePointerSectionOffset);
				IF ProtectModulesPointers THEN
					name := "Heaps.AnyPtr";
					offset := ToMemoryUnits(module.system,TypeRecordBaseOffset*module.system.addressSize);
					(* set base pointer *)
					NamedSymbolAt(modulePointerSection, modulePointerSectionOffset -1, name, NIL, 0, offset);
				END;
				ArrayBlock(modulePointerSection, modulePointerSizePC, "", TRUE);
				modulePointers := 0; 
				
				moduleNamePoolSection := Block("Heaps","SystemBlockDesc",".@ModuleNamePool", namePoolOffset);
				AddPointer(moduleNamePoolSection, namePoolOffset);
```
��� ���� ��� Heaps.AnyPtr �������� ���:
```
	(* a single pointer -- required as base type TD for array of pointer
		Don't rename this. Compiler refers to this TD by name
	*)
	AnyPtr = RECORD a: ANY END; 
```
����� ���� modulePointers. ������������ �����, ��� �� �������������:

```
		PROCEDURE AddPointer(section: IntermediateCode.Section; offset: LONGINT);
		BEGIN
			IF ~implementationVisitor.backend.cooperative THEN
				NamedSymbol(modulePointerSection, section.name, NIL, 0, offset);
				INC(modulePointers);
				(* optimization hint: this can be done once at the end but for consistency of the first tests we keep it like this *)
				PatchSize(modulePointerSection, modulePointerSizePC, modulePointers); 
			END;
		END AddPointer;
```

### ��� ���������� AddPointer? 

FoxIntermediateBackend - ����� ����� ������������ modulePointers := 0, 
��� ���� ������ ����������:
```
				moduleNamePoolSection := Block("Heaps","SystemBlockDesc",".@ModuleNamePool", namePoolOffset);
				AddPointer(moduleNamePoolSection, namePoolOffset);
```

� �����:
```
			PROCEDURE Export(CONST sections: ARRAY OF Sections.Section);
			VAR level, olevel, s: LONGINT; prev, this: Basic.SegmentedName; name: ARRAY 256 OF CHAR;
				scopes: ARRAY LEN(prev)+1 OF Scope; arrayName: ARRAY 32 OF CHAR;
				sym: Sections.Section; offset: LONGINT; symbol: Sections.Section;
				nextPatch: LONGINT;
			TYPE
				Scope = RECORD
					elements: LONGINT;
					gelements: LONGINT;
					section: IntermediateCode.Section;
					patchAdr: LONGINT;
					arraySizePC: LONGINT;
					beginPC: LONGINT; (* current scope start pc *)
				END;
			BEGIN
			  . . . . . . 
									IF scopes[level].section = NIL THEN
										arrayName := ".@ExportArray";
										Strings.AppendInt(arrayName, level);
										scopes[level].section := Block("Heaps","SystemBlockDesc",arrayName,offset);
										AddPointer(scopes[level].section,offset); 
										ArrayBlock(scopes[level].section,scopes[level].arraySizePC,"Modules.ExportDesc", FALSE);
```										
�
```
		PROCEDURE Module(bodyProc: IntermediateCode.Section);
				. . . . . . 
			typeInfoSection := Block("Heaps","SystemBlockDesc",".@TypeInfoArray",typeInfoSectionOffset);
			AddPointer(typeInfoSection, typeInfoSectionOffset);
			TypeInfoSection(typeInfoSection);
```
�� ���-�� ���? ��� ��� ��� ������� ANY � � ���� ������ ���� ����� �� ���������. 

�������� ���� - ���� �������� ���� Module.internal, �� ����� �����������. 
������ �������, ��� ����������. 

Heaps - ���������� ������ ��-�� CheckPointer, �� ���� �� �����������. 
��������� � � ��������. ��� ������ ��� ��� ����� ���, ��� � ��� ������, 
�.�. ����� ��, ��� �� �� ���������. ��������� �����������... 

� �����, �� ���� ����������� - ������:
* ������, �-��� ����� ������ � ���� �����, ���� ��������������� (��� RukhniNaModuleTchkInternal); 
* ������ ASSERT(Heaps.CheckPointer(pAddr)) � IzuchiTipEhtogoAny, �-���
������������, ��� � ���� ������ ��� �������� ���-�� ����������.

����� ���������� �����-���������-������-2017-09-17
