set -xe

SCRIPT_PATH=$(cd $(dirname $0) && pwd);
# Нормализируем путь, чтобы в нём не было .. 
export JAOS_ROOT=`readlink -f $SCRIPT_PATH/../../..`

cd $JAOS_ROOT
bash Linux32/skripty-sborki/Linux32-iz-WinAos/wsl-build-ehtap-1.sh
bash Linux32/skripty-sborki/Linux32-iz-WinAos/wsl-build-ehtap-2.sh
bash Linux32/skripty-sborki/Linux32-iz-WinAos/wsl-build-ehtap-3.sh
