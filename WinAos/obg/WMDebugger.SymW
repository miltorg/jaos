MODULE WMDebugger;
	IMPORT SYSTEM, WMMessages, KernelLog, Kernel32, Machine, Reflection, Objects, Modules, WMComponents, WM := WMWindowManager, WMStandardComponents, Strings, WMGraphics, WMRectangles, Files, Commands, WMWindowManager, UTF8Strings, WMTextView := WMTextView2, Texts, TextUtilities, Streams, WMTabComponents, BtDTraps, BtMenus, WMTrees, BtDbgPanel, WMComboBox, CompilerInterface, WMDiagnostics, BtDecoder, PodrobnajaPechatq;
CONST 
	Trace = TRUE; 
	MaxNbrOfTabs = 100; 
	MaxFrames = 32; 
	DecoderDisabled = TRUE; 
	AlignTop = WMComponents.AlignTop; 
	AlignLeft = WMComponents.AlignLeft; 
	AlignClient = WMComponents.AlignClient; 
	AlignRight = WMComponents.AlignRight; 
	AlignBottom = WMComponents.AlignBottom; 
	clBkgText = 3168854271; 
	clBkgTextCode = SIGNED32(3305103871); 
	clBkgTextActive = SIGNED32(4239234815); 
	clBkgToolBar = SIGNED32(3570452735); 
	dsBreak = 0; 
	dsCode = 2; 
	dsCurPC = 3; 
	dbgRun = 0; 
	dbgRestore = 1; 
	dbgTrace = 2; 
	dbgBreak = 3; 
	ShowAllProcs = TRUE; 
TYPE 
	Name = ARRAY 128 OF CHAR; 

	PVariable = POINTER TO RECORD 
		adr: ADDRESS; 
		mod: Modules.Module; 
		offset: SIZE; 
	END; 

	Filename = ARRAY 256 OF CHAR; 

	PosInfo = POINTER TO RECORD 
		pos: SIGNED32; 
		pc: ADDRESS; 
		BrkState: SET; 
		next: PosInfo; 
	END; 

	ProcInfo = POINTER TO RECORD 
		name: ARRAY 255 OF CHAR; 
		posList: PosInfo; 
		next: ProcInfo; 
		startOfs, lastOfs: ADDRESS; 
	END; 

	ModuleInfoArr = POINTER TO ARRAY OF PosInfo; 

	ModuleInfo = OBJECT {EXCLUSIVE} 
	VAR 
		procList: ProcInfo; 
		nofmi: SIGNED32; 
		mi: ModuleInfoArr; 
		mid: BtDecoder.ModuleInfo; 
		mod: Modules.Module; 
		saveCode: Modules.Bytes; 
		state: SIGNED32; 

		PROCEDURE ^  & Init*; 
		PROCEDURE ^ Sort*; 
		PROCEDURE ^ AddPos*(curPI: ProcInfo; pos, pc: ADDRESS); 
		PROCEDURE ^ Grow; 
		PROCEDURE ^ AddProc*(CONST name: ARRAY OF CHAR): ProcInfo; 
		PROCEDURE ^ Find*(CONST name: ARRAY OF CHAR; adr: ADDRESS): SIGNED32; 
		PROCEDURE ^ FindPC*(CONST name: ARRAY OF CHAR; adr: ADDRESS): BOOLEAN; 
		PROCEDURE ^ FindProc*(CONST name: ARRAY OF CHAR; VAR pi: PosInfo): BOOLEAN; 
		PROCEDURE ^ FindPos2(pos: SIGNED32): PosInfo; 
		PROCEDURE ^ FindPosInProc*(pos, len: SIGNED32): BOOLEAN; 
		PROCEDURE ^ FindPCinProc(pc: SIGNED32; VAR cur: ProcInfo; VAR pi: PosInfo): BOOLEAN; 
	END ModuleInfo; 

	DebugView = OBJECT {EXCLUSIVE} (WMTextView.TextView)
	VAR 
		m: ModuleInfo; 

		PROCEDURE ^ RenderLine*(canvas: WMGraphics.Canvas; VAR l: WMTextView.LineInfo; linenr, top, llen: SIZE); 
	END DebugView; 

	DbgPanel = OBJECT {EXCLUSIVE} (WMComponents.VisualComponent)
	VAR 
		dv: DebugView; 
		cvtext: Texts.Text; 
		filename: Filename; 
		module: ARRAY 256 OF CHAR; 
		owner: Window; 
		procInfo: ProcInfo; 
		vScrollbar: WMStandardComponents.Scrollbar; 

		PROCEDURE ^  & InitPanel*(window: Window); 
		PROCEDURE ^ DoCompile; 
	END DbgPanel; 

	Semaphore* = OBJECT {EXCLUSIVE} 
	VAR 
		busy: BOOLEAN; 

		PROCEDURE ^ Acquire*; 
		PROCEDURE ^ Release*; 
		PROCEDURE ^  & Init*; 
	END Semaphore; 

	Window* = OBJECT {EXCLUSIVE} (WMComponents.FormWindow)
	VAR 
		menuPanel: BtMenus.MenuPanel; 
		regPanel: BtDbgPanel.RegPanel; 
		tabs: WMTabComponents.Tabs; 
		pages: ARRAY MaxNbrOfTabs OF DbgPanel; 
		tabList: ARRAY MaxNbrOfTabs OF WMTabComponents.Tab; 
		currentPage: DbgPanel; 
		currentPageNr: SIGNED32; 
		combo: WMComboBox.ComboBox; 
		page: WMStandardComponents.Panel; 
		varpnl: BtDbgPanel.VariablesPanel; 
		mempnl: BtDbgPanel.MemPanel; 
		codepnl: BtDbgPanel.CodePanel; 
		semaphore: Semaphore; 
		dbgState: SET; 
		toolbar: WMStandardComponents.Panel; 

		PROCEDURE ^ CreateForm(): WMComponents.VisualComponent; 
		PROCEDURE ^  & New; 
		PROCEDURE ^ Close*; 
		PROCEDURE ^ ClickNode(sender, data: ANY); 
		PROCEDURE ^ Inspektirujj(nodeCaption: Strings.String; pv: PVariable); 
		PROCEDURE ^ GetLocalVars(int: Kernel32.Context; mod: Modules.Module); 
		PROCEDURE ^ ToFront*; 
		PROCEDURE ^ DbgHandler(VAR int: Kernel32.Context; VAR exc: Kernel32.ExceptionRecord; VAR handled: BOOLEAN); 
		PROCEDURE ^ onRun*(sender, data: ANY); 
		PROCEDURE ^ onGo*(sender, data: ANY); 
		PROCEDURE ^ Load(CONST filename: ARRAY OF CHAR); 
		PROCEDURE ^ Handle*(VAR m: WMMessages.Message); 
		PROCEDURE ^ RestoreCode(m: ModuleInfo); 
		PROCEDURE ^ ComboClick(sender, data: ANY); 
		PROCEDURE ^ OnBreak*(sender, data: ANY); 
		PROCEDURE ^ ShowExecutionPoint*(sender, data: ANY); 
		PROCEDURE ^ OnRunToCoursor*(sender, data: ANY); 
		PROCEDURE ^ OnStepInto*(sender, data: ANY); 
		PROCEDURE ^ OnStepOver*(sender, data: ANY); 
		PROCEDURE ^ OnRunUntilReturn*(sender, data: ANY); 
		PROCEDURE ^ OnToggleBP*(sender, data: ANY); 
		PROCEDURE ^ NewTab; 
		PROCEDURE ^ GetNrFromPage(page: DbgPanel): SIGNED32; 
		PROCEDURE ^ TabSelected(sender, data: ANY); 
		PROCEDURE ^ SelectTab(n: SIGNED32); 
		PROCEDURE ^ UpdatePages; 
	END Window; 
VAR 
	beginstr: ARRAY 10 OF CHAR; 
	endinstr: ARRAY 10 OF CHAR; 
	beginstrutf: ARRAY 10 OF SIGNED32; 
	endinstrutf: ARRAY 10 OF SIGNED32; 
	kw: Streams.Writer; 
	imgBrk: ARRAY 4 OF WMGraphics.Image; 
	activePI: PosInfo; 
	activePr: ProcInfo; 
	activeTab: SIGNED32; 
	winstance: Window; 
	idx: SIZE; 
	StrDbgPanel: Strings.String; 
	compiler: CompilerInterface.Compiler; 
	diagnostics: WMDiagnostics.Model; 

	PROCEDURE ^ GetProcOffset(CONST name: ARRAY OF CHAR; VAR adr: ADDRESS); 
	PROCEDURE ^ LoadModulePosInfo(CONST name: ARRAY OF CHAR; VAR m: ModuleInfo; CONST text: Texts.Text); 
	PROCEDURE ^ CheckHeapAddress(address: ADDRESS): BOOLEAN; 
	PROCEDURE ^ OnHeapOrStack(adr: ADDRESS; low, high: ADDRESS): BOOLEAN; 
	PROCEDURE ^ Open*(context: Commands.Context); 
	PROCEDURE ^ Cleanup; 
	PROCEDURE ^ CheckBP(bp: ADDRESS): ADDRESS; 
	PROCEDURE ^ LoadModule(CONST moduleName: ARRAY OF CHAR); 
BEGIN
END WMDebugger.
