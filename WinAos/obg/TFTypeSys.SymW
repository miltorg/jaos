MODULE TFTypeSys;
	IMPORT Strings, Trace, TFStringPool, Files, Streams, Tar, KernelLog;
CONST 
	TarBasedDB = FALSE; 
	SymVersion = 6; 
	TNone* =  -1; 
	TBasic* = 0; 
	TAlias* = 1; 
	TObject* = 2; 
	TArray* = 3; 
	TRecord* = 4; 
	TPointer* = 5; 
	TProcedure* = 6; 
	TEnum* = 7; 
	BasicBoolean* = 0; 
	BasicInt8* = 1; 
	BasicInt16* = 2; 
	BasicInt32* = 3; 
	BasicInt64* = 4; 
	BasicCard8* = 5; 
	BasicCard16* = 6; 
	BasicCard32* = 7; 
	BasicCard64* = 8; 
	BasicChar8* = 9; 
	BasicChar16* = 10; 
	BasicChar32* = 11; 
	BasicReal32* = 12; 
	BasicReal64* = 13; 
	BasicNIL* = 14; 
	BasicString* = 15; 
	BasicSet* = 16; 
	BasicArrayLiteral* = 17; 
	BasicAllRange* = 18; 
	BasicQuestion* = 19; 
	ExpressionIllegal* =  -1; 
	ExpressionPrimitive* = 0; 
	ExpressionUnary* = 1; 
	ExpressionBinary* = 2; 
	ExpressionProcedure* = 3; 
	ExpressionDesignator* = 4; 
	ExpressionAddressOf* = 5; 
	ExpressionSizeOf* = 6; 
	ExpressionRange* = 7; 
	IsParam* = 0; 
	IsVarParam* = 1; 
	IsConstParam* = 2; 
	OpNegate* = 1; 
	OpInvert* = 2; 
	OpAdd* = 3; 
	OpSub* = 4; 
	OpOr* = 5; 
	OpMul* = 6; 
	OpAnd* = 7; 
	OpIntDiv* = 8; 
	OpMod* = 9; 
	OpDiv* = 10; 
	OpEql* = 11; 
	OpNeq* = 12; 
	OpLss* = 13; 
	OpLeq* = 14; 
	OpGtr* = 15; 
	OpGeq* = 16; 
	OpIn* = 17; 
	OpIs* = 18; 
	StatementAssign* = 1; 
	ExportReadWrite* = 0; 
	ExportReadOnly* = 1; 
TYPE 
	String = Strings.String; 

	Position* = RECORD 
		valid*: BOOLEAN; 
		a*, b*: SIGNED32; 
	END; 

	Comment* = POINTER TO RECORD 
		next*: Comment; 
		pos*: Position; 
		str*: String; 
	END; 

	Comments* = POINTER TO RECORD 
		first*, last*: Comment; 
	END; 

	Expression* = POINTER TO RECORD 
		kind*, op*, basicType*: SIGNED32; 
		intValue*: SIGNED64; 
		strValue*: Strings.String; 
		setValue*: Set; 
		arrayLiteralValue*: ArrayLiteral; 
		a*, b*, c*: Expression; 
		designator*: Designator; 
		eol*: BOOLEAN; 
		isConstant*: BOOLEAN; 
		boolValue*: BOOLEAN; 
		next*: Expression; 
	END; 

	ExpressionList* = POINTER TO RECORD 
		next*: ExpressionList; 
		expression*: Expression; 
	END; 

	Designator* = POINTER TO RECORD (Expression)
	END; 

	Set* = POINTER TO RECORD 
		setRanges*: SetRange; 
	END; 

	SetRange* = POINTER TO RECORD 
		next*: SetRange; 
		a*, b*: Expression; 
	END; 

	ArrayLiteral* = POINTER TO RECORD 
		elements*: ArrayLiteralElement; 
	END; 

	ArrayLiteralElement* = POINTER TO RECORD 
		next*: ArrayLiteralElement; 
		val*: Expression; 
	END; 

	Ident* = POINTER TO RECORD (Designator)
		name*: SIZE; 
		type*: Type; 
		pos*: Position; 
	END; 

	Index* = POINTER TO RECORD (Designator)
		expressionList*: ExpressionList; 
	END; 

	Dereference* = POINTER TO RECORD (Designator)
	END; 

	ActualParameters* = POINTER TO RECORD (Designator)
		expressionList*: ExpressionList; 
	END; 

	Statement* = POINTER TO RECORD 
		next*: Statement; 
		preComment*, postComment*: Comments; 
	END; 

	EmptyStatement* = POINTER TO RECORD (Statement)
	END; 

	Assignment* = POINTER TO RECORD (Statement)
		designator*: Designator; 
		expression*: Expression; 
	END; 

	ProcedureCall* = POINTER TO RECORD (Statement)
		designator*: Designator; 
	END; 

	IFStatement* = POINTER TO RECORD (Statement)
		expression*: Expression; 
		then*, else*: Statement; 
	END; 

	WHILEStatement* = POINTER TO RECORD (Statement)
		expression*: Expression; 
		statements*: Statement; 
	END; 

	FORStatement* = POINTER TO RECORD (Statement)
		variable*: Designator; 
		fromExpression*, toExpression*, byExpression*: Expression; 
		statements*: Statement; 
	END; 

	WITHStatement* = POINTER TO RECORD (Statement)
		variable*: Ident; 
		typedClauses*: WITHStatementPart; 
		elseClause*: Statement; 
	END; 

	WITHStatementPart* = POINTER TO RECORD (Statement)
		parent*: WITHStatement; 
		next*: WITHStatementPart; 
		scope*: Scope; 
		type*: Type; 
		statements*: Statement; 
		namedObject*: WithStatementPartNamedObject; 
	END; 

	REPEATStatement* = POINTER TO RECORD (Statement)
		expression*: Expression; 
		statements*: Statement; 
	END; 

	LOOPStatement* = POINTER TO RECORD (Statement)
		statements*: Statement; 
	END; 

	RETURNStatement* = POINTER TO RECORD (Statement)
		expression*: Expression; 
	END; 

	IGNOREStatement* = POINTER TO RECORD (Statement)
		expression*: Expression; 
	END; 

	AWAITStatement* = POINTER TO RECORD (Statement)
		expression*: Expression; 
	END; 

	EXITStatement* = POINTER TO RECORD (Statement)
	END; 

	CASEStatement* = POINTER TO RECORD (Statement)
		expression*: Expression; 
		cases*: Case; 
		else*: Statement; 
	END; 

	Case* = POINTER TO RECORD 
		next*: Case; 
		caseRanges*: CaseRange; 
		statements*: Statement; 
	END; 

	CaseRange* = POINTER TO RECORD 
		next*: CaseRange; 
		a*, b*: Expression; 
	END; 

	StatementBlock* = POINTER TO RECORD (Statement)
		statements*: Statement; 
	END; 

	Array* = POINTER TO RECORD 
		container*: Scope; 
		open*: BOOLEAN; 
		len*: SIGNED32; 
		expression*: Expression; 
		base*: Type; 
	END; 

	Pointer* = POINTER TO RECORD 
		type*: Type; 
	END; 

	ProcedureSignature* = POINTER TO RECORD 
		params*: ObjectList; 
		return*: Type; 
	END; 

	ProcedureType* = POINTER TO RECORD 
		signature*: ProcedureSignature; 
	END; 

	Type* = POINTER TO RECORD 
		container*: Scope; 
		kind*: SIGNED32; 
		basicType*: SIGNED32; 
		qualident*: Designator; 
		type*: TypeDecl; 
		array*: Array; 
		record*: Record; 
		enum*: Enum; 
		pointer*: Pointer; 
		object*: Class; 
		procedure*: ProcedureType; 
	END; 

	NamedObject* = POINTER TO RECORD 
		container*, scope*: Scope; 
		name*: String; 
		exportState*: SET; 
		preComment*, postComment*: Comments; 
		pos*, altPos*: Position; 
	END; 

	TypeDecl* = POINTER TO RECORD (NamedObject)
		type*: Type; 
	END; 

	Const* = POINTER TO RECORD (NamedObject)
		expression*: Expression; 
	END; 

	Import* = POINTER TO RECORD (NamedObject)
		import*: String; 
		package*: String; 
	END; 

	Var* = POINTER TO RECORD (NamedObject)
		type*: Type; 
		varNr*: SIGNED32; 
		parameterType*: SET; 
		initialValue*: Expression; 
	END; 

	WithStatementPartNamedObject* = POINTER TO RECORD (NamedObject)
		statement*: WITHStatementPart; 
	END; 

	NamedObjectArray = POINTER TO ARRAY OF NamedObject; 

	ObjectList* = OBJECT 
	VAR 
		objs-: NamedObjectArray; 
		nofObjs-: SIGNED32; 

		PROCEDURE ^  & Init*; 
		PROCEDURE ^ Add*(m: NamedObject); 
		PROCEDURE ^ AddReplace*(m: NamedObject); 
		PROCEDURE ^ Get*(CONST name: ARRAY OF CHAR): NamedObject; 
		PROCEDURE ^ GetWithPrefix*(CONST prefix: ARRAY OF CHAR; candidates: ObjectList; onlyPublic: BOOLEAN); 
	END ObjectList; 

	Scope* = OBJECT 
	VAR 
		elements*, params*: ObjectList; 
		parent*, super*: Scope; 
		superQualident*: Designator; 
		ownerBody*: Statement; 
		owner*: NamedObject; 
		counterOfWithStatements*: SIGNED16; 

		PROCEDURE ^  & Init*; 
		PROCEDURE ^ Add*(no: NamedObject); 
		PROCEDURE ^ FixSuperScope*; 
		PROCEDURE ^ Find*(VAR name: ARRAY OF CHAR; searchUpscopes: BOOLEAN): NamedObject; 
		PROCEDURE ^ FindCandidates*(VAR prefix: ARRAY OF CHAR; searchUpscopes, onlyPublic: BOOLEAN; candidates: ObjectList); 
	END Scope; 

	Record* = POINTER TO RECORD 
		scope*: Scope; 
	END; 

	Enum* = POINTER TO RECORD 
		scope*: Scope; 
	END; 

	ProcDecl* = POINTER TO RECORD (NamedObject)
		signature*: ProcedureSignature; 
		extern*: BOOLEAN; 
	END; 

	Class* = POINTER TO RECORD (NamedObject)
		implements*: ObjectList; 
	END; 

	Module* = POINTER TO RECORD (NamedObject)
		package*: Strings.String; 
		filename*: Strings.String; 
		isSymbolic*: BOOLEAN; 
	END; 

	NameSpace* = OBJECT 
	VAR 
		modules: ObjectList; 

		PROCEDURE ^  & Init*; 
		PROCEDURE ^ AddModule*(m: Module); 
		PROCEDURE ^ GetModule*(CONST name: ARRAY OF CHAR): Module; 
	END NameSpace; 

	FailList = POINTER TO RECORD 
		next: FailList; 
		name: Strings.String; 
	END; 
VAR 
	s*: TFStringPool.StringPool; 
	xymSubdir: Strings.String; 
	ns*: NameSpace; 
	db: Tar.Archive; 
	failList: FailList; 

	PROCEDURE ^ GetModule*(imp: Import): Module; 
	PROCEDURE ^ FindType*(d: Designator; scope: Scope): Type; 
	PROCEDURE ^ FindBasicTypeByName*(str: ARRAY 64 OF CHAR; scope: Scope): Type; 
	PROCEDURE ^ PrimitiveExpressionInt*(value: SIGNED64): Expression; 
	PROCEDURE ^ PrimitiveExpressionString*(CONST str: ARRAY OF CHAR): Expression; 
	PROCEDURE ^ PrimitiveExpressionBool*(value: BOOLEAN): Expression; 
	PROCEDURE ^ PrimitiveExpressionSet*(value: Set): Expression; 
	PROCEDURE ^ PrimitiveExpressionArray*(value: ArrayLiteral): Expression; 
	PROCEDURE ^ PrimitiveExpressionNIL*(): Expression; 
	PROCEDURE ^ PrimitiveExpressionAllRange*(): Expression; 
	PROCEDURE ^ PrimitiveExpressionQuestion*(): Expression; 
	PROCEDURE ^ IllegalExpression*(): Expression; 
	PROCEDURE ^ UnaryExpression*(op: SIGNED16; exp: Expression): Expression; 
	PROCEDURE ^ BinaryExpression*(op: SIGNED32; expa, expb: Expression): Expression; 
	PROCEDURE ^ RangeExpression*(expa, expb, expby: Expression): Expression; 
	PROCEDURE ^ CreateEmptyDesignatorExpression*(): Designator; 
	PROCEDURE ^ CreateEmptyIdentExpression*(): Ident; 
	PROCEDURE ^ CreateEmptyIndexExpression*(): Index; 
	PROCEDURE ^ CreateEmptyActualParametersExpression*(): ActualParameters; 
	PROCEDURE ^ CreateEmptyDereferenceExpression*(): Dereference; 
	PROCEDURE ^ CreateDesignatorExpressionForAddress*(designator: Designator): Expression; 
	PROCEDURE ^ CreateAddressOfExpression*(designator: Designator): Expression; 
	PROCEDURE ^ CreateSizeOfExpression*(designator: Designator): Expression; 
	PROCEDURE ^ CreateAssignment*(designator: Designator; expression: Expression): Statement; 
	PROCEDURE ^ CreateProcedureCall*(designator: Designator): Statement; 
	PROCEDURE ^ CreateWhile*(expression: Expression; statements: Statement): Statement; 
	PROCEDURE ^ CreateRepeat*(expression: Expression; statements: Statement): Statement; 
	PROCEDURE ^ CreateLoop*(statements: Statement): Statement; 
	PROCEDURE ^ CreateFor*(variable: Designator; from, to, by: Expression; statements: Statement): Statement; 
	PROCEDURE ^ CreateScopeAndNamedObjectForWithStatement*(cs: Scope; variable: Ident; type: Type; VAR innerScope: Scope; VAR wsno: WithStatementPartNamedObject); 
	PROCEDURE ^ SynthesizeVariableForWithStatement(innerScope: Scope; variable: Designator; type: Type); 
	PROCEDURE ^ CreateCase*(expression: Expression; cases: Case; statements: Statement): Statement; 
	PROCEDURE ^ CreateExit*(): Statement; 
	PROCEDURE ^ CreateReturn*(ex: Expression): Statement; 
	PROCEDURE ^ CreateAwait*(ex: Expression): Statement; 
	PROCEDURE ^ CreateIgnore*(designator: Designator): Statement; 
	PROCEDURE ^ AddComment*(VAR comments: Comments; CONST str: ARRAY OF CHAR): Comment; 
	PROCEDURE ^ NewEmptyStatement*(): Statement; 
	PROCEDURE ^ ExportQualident(w: Streams.Writer; ident: Designator; scope: Scope); 
	PROCEDURE ^ ExportSignature(w: Streams.Writer; signature: ProcedureSignature; scope: Scope); 
	PROCEDURE ^ ExportType(w: Streams.Writer; t: Type; level: SIGNED32; scope: Scope); 
	PROCEDURE ^ ExportScope(w: Streams.Writer; scope: Scope; level: SIGNED32); 
	PROCEDURE ^ WriteSymbolFile*(m: Module); 
	PROCEDURE ^ ImportQualident(r: Streams.Reader): Designator; 
	PROCEDURE ^ ImportSignature(r: Streams.Reader; scope: Scope; owner: NamedObject): ProcedureSignature; 
	PROCEDURE ^ ImportType(r: Streams.Reader; scope: Scope; owner: NamedObject): Type; 
	PROCEDURE ^ ImportScope(r: Streams.Reader; scope: Scope; owner: NamedObject): Scope; 
	PROCEDURE ^ ReadSymbolFile*(CONST modname: ARRAY OF CHAR): Module; 
	PROCEDURE ^ OpenDB; 
	PROCEDURE ^ ClearSymbolCache*; 
	PROCEDURE ^ set_xymSubdir; 
BEGIN
END TFTypeSys.
