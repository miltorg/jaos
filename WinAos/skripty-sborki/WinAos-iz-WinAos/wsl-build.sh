set -xe

SCRIPT_PATH=$(cd $(dirname $0) && pwd);
# Нормализируем путь, чтобы в нём не было .. 
export JAOS_ROOT=`readlink -f $SCRIPT_PATH/../../..`

cd $JAOS_ROOT
bash WinAos/skripty-sborki/WinAos-iz-WinAos/wsl-build-ehtap-1.sh
bash WinAos/skripty-sborki/WinAos-iz-WinAos/wsl-build-ehtap-2.sh
bash WinAos/skripty-sborki/WinAos-iz-WinAos/wsl-build-ehtap-3.sh
