(** AUTHOR "rg"; PURPOSE "Decoder for binary executable code"; *)
(** bohdant: adaptation for debugger (12.2014) removing visual part and make more visible fields*)

MODULE BtDecoder;

IMPORT SYSTEM, Modules, Streams, MemoryReader, Strings, Files, KernelLog, TextUtilities,  Texts;

CONST
	maxDecoders = 5;

	MaxOpcodeSize = 20; (* enough for IA32 *)

	OFFHdrRef = 8CX;


TYPE
	Opcode* = OBJECT
		VAR
			instr* : SIGNED32; (* instruction code *)
			offset* : SIGNED32;
			code* : Modules.Bytes;
			length- : SIGNED32;
			decoder* : Decoder;
			next*: Opcode;
			stream : Streams.Writer;
			proc- : ProcedureInfo;

		PROCEDURE &New* (proc : ProcedureInfo; stream : Streams.Writer);
		BEGIN
			length := 0;
			SELF.proc := proc;
			SELF.stream := stream
		END New;

		PROCEDURE PrintOpcodeBytes* (w : Streams.Writer);
		END PrintOpcodeBytes;

		PROCEDURE PrintInstruction* (w : Streams.Writer);
		END PrintInstruction;

		PROCEDURE PrintArguments* (w : Streams.Writer);
		END PrintArguments;

		PROCEDURE PrintVariables* (w : Streams.Writer);
		END PrintVariables;

		PROCEDURE ToString* () : Strings.String;
		VAR
			str : ARRAY 255 OF CHAR;
			temp : ARRAY 10 OF CHAR;
		BEGIN
			Strings.IntToStr(instr, temp);
			Strings.Append(str, "Opcode: instr = "); Strings.Append(str, temp);
			Strings.IntToHexStr(offset, 0, temp);
			Strings.Append(str, ", offset = "); Strings.Append(str, temp);
			RETURN Strings.NewString(str)
		END ToString;

		PROCEDURE WriteHex8* (x : SIGNED32; w : Streams.Writer);
		VAR result : ARRAY 3 OF CHAR;
		BEGIN
			IntToHex(x, 2, result); w.String(result)
		END WriteHex8;

		PROCEDURE WriteHex16* (x : SIGNED32; w : Streams.Writer);
		VAR result : ARRAY 5 OF CHAR;
		BEGIN
			IntToHex(x, 4, result); w.String(result)
		END WriteHex16;

		PROCEDURE WriteHex32* (x : SIGNED32; w : Streams.Writer);
		VAR result : ARRAY 10 OF CHAR;
		BEGIN
			IntToHex(x, 8, result); w.String(result)
		END WriteHex32;
	END Opcode;

	Decoder* = OBJECT
		VAR
			codeBuffer : Modules.Bytes;
			reader: Streams.Reader;
			outputStreamWriter* : Streams.Writer;
			firstOpcode, lastOpcode, currentOpcode: Opcode;
			completed : BOOLEAN;
			currentBufferPos, currentCodePos, opcodes, mode : SIGNED32;
			currentProc* : ProcedureInfo;

		PROCEDURE &New* (reader : Streams.Reader);
		BEGIN
			SELF.reader := reader;
			SELF.mode := mode;
			NEW(codeBuffer, MaxOpcodeSize);  (* limit: maximum # bytes per opcode *)
			currentCodePos := 0;
			opcodes := 0;
			completed := FALSE
		END New;

		PROCEDURE Bug* (op, no: SIGNED32);
		BEGIN
			KernelLog.Ln;  KernelLog.String("*** decode error ***; "); KernelLog.String("op = "); KernelLog.Hex(op, -1); KernelLog.String(", no = "); KernelLog.Int(no, 0); KernelLog.Ln;
			completed := TRUE
		END Bug;

		PROCEDURE NewOpcode* () : Opcode;
		VAR
			opcode : Opcode;
		BEGIN
			NEW(opcode, currentProc, outputStreamWriter);
			RETURN opcode
		END NewOpcode;

		PROCEDURE DecodeThis* (opcode : Opcode);
		END DecodeThis;

		PROCEDURE Decode* (proc : ProcedureInfo) : Opcode;
		(*
		VAR
			str : Strings.String;
		*)
		BEGIN
			currentProc := proc;
			WHILE ~completed DO
				currentBufferPos := 0;
				IF reader.Available() > 0 THEN
					currentOpcode := NewOpcode();
					BEGIN {EXCLUSIVE}
						DecodeThis(currentOpcode);
						IF reader.res = Streams.Ok THEN
							IF lastOpcode = NIL THEN
								lastOpcode := currentOpcode;
								firstOpcode := currentOpcode;
							ELSE
								lastOpcode.next := currentOpcode;
								lastOpcode := currentOpcode
							END;
							currentOpcode.offset := currentCodePos+proc.codeOffset;
							(*
							str := currentOpcode.ToString();
							KernelLog.String(str^); KernelLog.Ln;
							*)
							INC(currentOpcode.length);
							(* copy all buffered bytes to the opcode *)
							CopyBufferToOpcode(currentOpcode);
							INC(opcodes)
						END
					END
				ELSE
					completed := TRUE
				END;
				IF reader.res # Streams.Ok THEN completed := TRUE END;
			END;
			RETURN firstOpcode
		END Decode;

		PROCEDURE CopyBufferToOpcode(opcode : Opcode);
		VAR i : SIGNED32;
		BEGIN
			NEW(opcode.code, currentBufferPos);
			FOR i := 0 TO currentBufferPos-1 DO
				opcode.code[i] := codeBuffer[i]
			END;
			opcode.length := currentBufferPos;
			INC(currentCodePos, currentBufferPos)
		END CopyBufferToOpcode;

		PROCEDURE InsertBytesAtBufferHead* (bytes : Modules.Bytes);
		VAR i, n : SIGNED32;
		BEGIN
			n := LEN(bytes);
			FOR i := currentBufferPos-1 TO 0 BY -1 DO
				codeBuffer[i+n] := codeBuffer[i]
			END;
			FOR i := 0 TO n-1 DO
				codeBuffer[i] := bytes[i]
			END;
			INC(currentBufferPos, n)
		END InsertBytesAtBufferHead;

		PROCEDURE ReadChar* () : CHAR;
		VAR
			ch : CHAR;
		BEGIN
			reader.Char(ch);
			IF reader.res = Streams.Ok THEN
				codeBuffer[currentBufferPos] := ch;
				INC(currentBufferPos);
			END;
			RETURN ch
		END ReadChar;

		PROCEDURE ReadInt* () : SIGNED16;
		VAR
			i : SIGNED16;
		BEGIN
			reader.RawInt(i);
			IF reader.res = Streams.Ok THEN
				SYSTEM.MOVE(ADDRESSOF(i), ADDRESSOF(codeBuffer[currentBufferPos]), 2);
				INC(currentBufferPos, 2)
			END;
			RETURN i
		END ReadInt;

		PROCEDURE ReadLInt* () : SIGNED32;
		VAR
			l, highByte, base : SIGNED32;
			ch : CHAR;
		BEGIN
			ch := ReadChar();
			l := LONG(ORD(ch));
			ch := ReadChar();
			l := l + LONG(ORD(ch)) * 100H;
			ch := ReadChar();
			l := l + LONG(ORD(ch)) * 10000H;
			ch := ReadChar();
			highByte := ORD(ch);
			IF highByte >= 128 THEN base := MIN(SIGNED32); DEC(highByte, 128) ELSE base := 0 END;
			l := base + highByte * 1000000H + l;
			RETURN l
		END ReadLInt;
	END Decoder;

	DecoderFactory = PROCEDURE {DELEGATE} (reader : Streams.Reader) : Decoder;

	Info = OBJECT
		VAR
			name-: ARRAY 256 OF CHAR;
	END Info;

	FieldInfo* = OBJECT (Info) END FieldInfo;

	FieldArray = POINTER TO ARRAY OF FieldInfo;

	ProcedureInfo* = OBJECT (Info)
		VAR
			codeOffset, codeSize, retType-, index : SIGNED32;
			fields : FieldArray;
			fieldCount- : SIGNED32;
			method : BOOLEAN;

			gcInfo: GCInfo;

		PROCEDURE &New (CONST n : ARRAY OF CHAR; ofs, idx : SIGNED32);
		BEGIN
			COPY (n, name);
			codeOffset := ofs;
			index := idx;
			method := FALSE;
			NEW(fields, 5);
			gcInfo := NIL;
		END New;

		PROCEDURE GetFieldAtOffset*(offset : SIGNED32) : FieldInfo;
		BEGIN
		HALT(30336) (* ustarelo, skopirujj zanovo s odnoimennogo koda v Decoder.Mod *)
		END GetFieldAtOffset;
	END ProcedureInfo;

	ProcedureArray = POINTER TO ARRAY OF ProcedureInfo;

	TypeInfo* = OBJECT (Info)
		VAR
			procedures : ProcedureArray;
			fields : FieldArray;
			procedureCount, fieldCount : SIGNED32;

		PROCEDURE &New (CONST n : ARRAY OF CHAR);
		BEGIN
			COPY (n, name);
			procedureCount := 0;
			fieldCount := 0;
			NEW(procedures, 5);
			NEW(fields, 5)
		END New;

	END TypeInfo;

	TypeArray = POINTER TO ARRAY OF TypeInfo;

	Export*=POINTER TO RECORD
		next: Export;
		fp: SIGNED32;
		val: SIGNED32;
	END;

	Use= POINTER TO RECORD
		next: Use;
		fp: SIGNED32;
		val: SIGNED32;
		name: ARRAY 256 OF CHAR;
	END;

	Import=OBJECT
	VAR
		next: Import;
		name: ARRAY 256 OF CHAR;
		uses: Use;

		PROCEDURE AddUse(u: Use);
		VAR x: Use;
		BEGIN
			IF uses = NIL THEN uses := u
			ELSE x := uses; WHILE x.next # NIL DO x := x.next; END;
			x.next := u;
			END;
		END AddUse;

	END Import;

	VarConstLink=RECORD
		num: SIGNED32;
		ch: CHAR;
		links: POINTER TO ARRAY OF SIGNED32;
	END;

	Link=RECORD
		num: SIGNED32;
	END;

	Entry=RECORD
		num: SIGNED32;
	END;

	GCInfo= POINTER TO RECORD
		codeOffset, beginOffset, endOffset: SIGNED32;
		pointers: POINTER TO ARRAY OF SIGNED32
	END;

	ObjHeader = RECORD (* data from object file header *)
		entries, commands, pointers, types, modules: SIGNED32;
		codeSize, dataSize, refSize, constSize, exTableLen, crc: SIGNED32;
	END;

	ModuleInfo* = OBJECT (Info)
		VAR
			module : Modules.Module;
			header: ObjHeader;
			procedures : ProcedureArray; (* references to all procedures, including methods *)
			procedureCount : SIGNED32;
			types : TypeArray;
			typeCount : SIGNED32;
			currentProcInfo : ProcedureInfo;
			ext : Extension;
			codeScaleCallback: CodeScaleCallback;

			exports: Export;
			imports: Import;
			varConstLinks: POINTER TO ARRAY OF VarConstLink;
			links: POINTER TO ARRAY OF Link;
			entries: POINTER TO ARRAY OF Entry;
			gcInfo: POINTER TO ARRAY OF GCInfo;

		PROCEDURE AddExport(e: Export);
		VAR x: Export;
		BEGIN
			IF exports = NIL THEN exports := e
			ELSE
				x := exports;
				WHILE x.next # NIL DO x := x.next END;
				x.next := e;
			END;
		END AddExport;

		PROCEDURE AddImport(i: Import);
		VAR x: Import;
		BEGIN
			IF imports = NIL THEN imports := i
			ELSE
				x := imports;
				WHILE x.next # NIL DO x := x.next END;
				x.next := i;
			END;
		END AddImport;


		PROCEDURE GetOpcodes* (proc : ProcedureInfo) : Opcode;
		VAR
			reader : MemoryReader.Reader;
			ofs : SIGNED32;
			decoder : Decoder;
		BEGIN
			ofs := ADDRESSOF(module.code[proc.codeOffset]);
			NEW(reader, ofs, proc.codeSize);
			decoder := GetDecoder(ext, reader);
			RETURN decoder.Decode(proc)
		END GetOpcodes;

		PROCEDURE AddProcedure (procInfo : ProcedureInfo);
		VAR
			oldProcs : ProcedureArray;
			i, len : SIGNED32;
		BEGIN
			IF procedureCount = LEN(procedures) THEN
				oldProcs := procedures;
				len := LEN(procedures);
				NEW(procedures, 2 * len);
				FOR i := 0 TO len-1 DO procedures[i] := oldProcs[i] END
			END;
			procedures[procedureCount] := procInfo;
			INC(procedureCount)
		END AddProcedure;


		PROCEDURE GetProcedureByIndex *(idx : SIGNED32) : ProcedureInfo;
		VAR
			i : SIGNED32;
		BEGIN
			i := 0;
			WHILE i < procedureCount DO
				IF idx = procedures[i].index THEN
					RETURN procedures[i]
				END;
				INC(i)
			END;
			RETURN NIL
		END GetProcedureByIndex;

		PROCEDURE DecodeRefs(reader : Streams.Reader);
			BEGIN
			HALT(30336); (* ustarelo, skopirujj zanovo s odnoimennogo koda v Decoder.Mod *)
			RETURN END DecodeRefs;

		PROCEDURE FindProcedureFromPC*(pc : UNSIGNED32) : ProcedureInfo;
		VAR
			i : SIGNED32;
		BEGIN
			ASSERT(procedures # NIL);
			WHILE i < procedureCount DO
				IF (pc >= procedures[i].codeOffset) & (pc < procedures[i].codeOffset + procedures[i].codeSize) THEN
					RETURN procedures[i]
				END;
				INC(i)
			END;
			RETURN NIL
		END FindProcedureFromPC;

		PROCEDURE Init;
		BEGIN
			NEW(module);
			procedureCount := 0;
			NEW(procedures, 5);
			codeScaleCallback := NIL;
			ext := ""
		END Init;

		PROCEDURE OutlineNamedProcedure*(CONST name : ARRAY OF CHAR;w : Streams.Writer);
		VAR found : BOOLEAN;
			i : SIGNED32;
		BEGIN
			i := 0; found := FALSE;
			WHILE ~found & (i < procedureCount) DO
				IF procedures[i].name = name THEN
					found := TRUE;
					OutlineProcedure(procedures[i],w)
				END;
				INC(i)
			END;
			IF ~found THEN
				KernelLog.String("Decoder: ERROR: OutlineNamedProcedure: Procedure not found: "); KernelLog.String(name); KernelLog.Ln
			END
		END OutlineNamedProcedure;

		PROCEDURE OutlineProcedure*(proc : ProcedureInfo;w : Streams.Writer);
			VAR
				s : Strings.String;
				opcodes : Opcode;
				
				i : SIGNED32;
(* ug *)			s2: Strings.String;
		BEGIN
			currentProcInfo := proc;

			w.String(currentProcInfo.name);
			w.String(":");
(* ug *)		w.Ln; w.String("codeOffset = "); NEW(s2, 20); IntToHex(currentProcInfo.codeOffset, 8, s2^); Strings.Append(s2^, "H"); w.String(s2^);
			w.Ln; w.Ln;

			(* output data *)
			opcodes := GetOpcodes(currentProcInfo);
			WHILE opcodes # NIL DO

				NEW(s, 20);
				IntToHex(opcodes.offset, 8, s^);
				Strings.Append(s^, "H");
				w.String(s^); w.Char(9X);
				(* insert marker for pc if selected *)
				opcodes.PrintOpcodeBytes(w); 	w.Char(9X);

				opcodes.PrintInstruction(w);	w.Char(9X);
				opcodes.PrintArguments(w); w.Char(9X);

				opcodes.PrintVariables(w);
				w.Ln;
				opcodes := opcodes.next
			END;

			w.Update;

			IF proc.gcInfo # NIL THEN
				w.Ln;
				w.String("pcFrom="); w.Hex(proc.gcInfo.codeOffset,1);w.Ln;
				w.String("gcEnd="); w.Hex(proc.gcInfo.endOffset,1);w.Ln;
				w.String("gcBegin="); w.Hex(proc.gcInfo.beginOffset,1);w.Ln;
				FOR i := 0 TO LEN(proc.gcInfo.pointers)-1 DO
					w.String("ptr @ "); w.Int(proc.gcInfo.pointers[i],1); w.Ln;
				END;
			END;
			w.Update;

		END OutlineProcedure;


	END ModuleInfo;

	CodeScaleCallback* = PROCEDURE(VAR size : SIGNED32);

	ModuleInfoObjectFile* = OBJECT (ModuleInfo)
	VAR
		f: Files.File;
		r : Files.Reader;
		version : SIGNED32;
		nofLinks, nofVarConstLinks : SIGNED32;
		symSize : SIGNED32;
		noProcs : SIGNED32; (* ug: temporary *)

		PROCEDURE DecodeEntries;
		VAR
			ch : CHAR; i, e : SIGNED32;
		BEGIN
			ch := r.Get();
			ASSERT(ch = 82X);
			NEW(module.entry, header.entries);
			FOR i := 0 TO header.entries-1 DO
				r.RawNum(e);
				module.entry[i] := e
			END
		END DecodeEntries;

		PROCEDURE SkipCommands;
		VAR
			ch : CHAR;
			i, num : SIGNED32;
			n : Modules.Name;
		BEGIN
			ch := r.Get();
			ASSERT(ch = 83X);
			FOR i := 0 TO header.commands-1 DO
				r.RawNum(num); r.RawNum(num); r.RawString(n); r.RawNum(num)
			END
		END SkipCommands;

		PROCEDURE SkipPointers;
		VAR
			ch : CHAR;
			i, num : SIGNED32;
		BEGIN
			ch := r.Get();
			ASSERT(ch = 84X);
			FOR i := 0 TO header.pointers-1 DO
				r.RawNum(num)
			END
		END SkipPointers;

		PROCEDURE SkipImports;
		VAR
			ch : CHAR;
			i : SIGNED32;
			n : Modules.Name;
		BEGIN
			ch := r.Get();
			ASSERT(ch = 85X);
			FOR i := 0 TO header.modules-1 DO
				r.RawString(n)
			END
		END SkipImports;

		PROCEDURE SkipVarConstLinks;
		VAR
			ch : CHAR;
			i, j, num, count : SIGNED32;
		BEGIN
			ch := r.Get();
			ASSERT(ch = 8DX);
			NEW(varConstLinks,nofVarConstLinks);
			FOR i := 0 TO nofVarConstLinks-1 DO
				ch := r.Get();
				r.RawNum(num);
				r.RawLInt(count);
				varConstLinks[i].num := i;
				varConstLinks[i].ch := ch;
				NEW(varConstLinks[i].links,count);
				FOR j := 0 TO count-1 DO
					r.RawNum(num);
					varConstLinks[i].links[j] := num;
				END
			END
		END SkipVarConstLinks;

		PROCEDURE SkipLinks;
		VAR
			ch : CHAR;
			i, num : SIGNED32;
		BEGIN
			ch := r.Get();
			ASSERT(ch = 86X);
			NEW(links,nofLinks);
			FOR i := 0 TO nofLinks-1 DO
				r.SkipBytes(2);  (* skip 2 characters *)
				r.RawNum(num);
				links[i].num := num;
			END;
			NEW(entries,header.entries);
			FOR i := 0 TO header.entries-1 DO
				r.RawNum(num);
				entries[i].num := num;
			END;
			r.RawNum(num)
		END SkipLinks;

		PROCEDURE SkipConsts;
		VAR
			ch : CHAR; i: SIGNED32;
		BEGIN
			ch := r.Get();
			ASSERT(ch = 87X);
			NEW(module.data,header.constSize);
			FOR i := 0 TO header.constSize-1 DO
				r.Char(module.data[i]);
			END;
		END SkipConsts;

		PROCEDURE SkipExports;
		VAR count: SIGNED32; ch : CHAR;

			PROCEDURE LoadScope (level: SIGNED32);
			VAR adr, fp, off, len, exp: SIGNED32; check: POINTER TO ARRAY OF SIGNED32;export: Export;
			BEGIN
				r.RawLInt(exp);
				r.RawNum(fp);
				len := 0;
				IF fp # 0 THEN NEW(check, exp) END;
				WHILE fp # 0 DO
					NEW(export);
					export.fp := fp;
					AddExport(export);
					IF (fp = 1) THEN
						r.RawNum(off);
						export.val := off;
						IF off >= 0 THEN
							INC(count);
							LoadScope (level+1(*1*))
						END
					ELSE
						IF level = 0 THEN
							r.RawNum(adr);
							export.val := adr;
							(*
							i := 0;
							WHILE i # len DO
								IF check[i] = fp THEN
									r.RawString(name);
									COPY(name,export.name);
									i := len
								ELSE
									INC(i)
								END
							END;
							*)
							check[len] := fp; INC(len)
						END;
					END;
					r.RawNum(fp)
				END
			END LoadScope;

		BEGIN
			ch := r.Get();
			ASSERT(ch = 88X);
			LoadScope (0)
		END SkipExports;

		PROCEDURE SkipUse;
		VAR ch : CHAR;

			PROCEDURE ReadUsedModules;
			VAR name : Modules.Name;					import: Import;

				PROCEDURE ReadEntry;
				VAR
					fp, arg : SIGNED32;
					name : ARRAY 256 OF CHAR;
					use: Use;
				BEGIN
					r.RawNum(fp);
					r.RawString(name);
					r.RawNum(arg);
					NEW(use);
					use.fp := fp;
					COPY(name,use.name);
					use.val := arg;
					import.AddUse(use);
					IF arg > 0 THEN
						IF r.Peek() = 1X THEN
							ch := r.Get();
							r.RawNum(arg)
						END
					ELSIF arg < 0 THEN
					ELSE
						IF r.Peek() = 1X THEN
							(* read used record *)
							ch := r.Get();
							r.RawNum(arg); (* tdentry *)
							IF r.Peek() # 0X THEN
								r.RawNum(arg); (* FP *)
								r.RawString(name);
								ASSERT(name = "@");
							END;
							ch := r.Get();
							ASSERT(ch = 0X)
						END;
					END
				END ReadEntry;

			BEGIN
				WHILE r.Peek() # 0X DO
					r.RawString(name);
					NEW(import);
					COPY(name,import.name);
					AddImport(import);
					WHILE r.Peek() # 0X DO
						ReadEntry
					END;
					ch := r.Get();
					ASSERT(ch = 0X)
				END
			END ReadUsedModules;

		BEGIN
			ch := r.Get();
			ASSERT(ch = 08AX);
			ReadUsedModules;
			ch := r.Get();
			ASSERT(ch = 0X)
		END SkipUse;

		PROCEDURE DecodeTypes;
		VAR
			i, j, size, entry, ptrOfs, tdaddr, moduleBase, nofMethods, nofInhMethods, nofNewMethods, nofPointers, tdSize (* ug *), methNr, entryNr : SIGNED32;
			name : ARRAY 256 OF CHAR; ch : CHAR;
			type : TypeInfo;
			procInfo : ProcedureInfo;
		BEGIN
			ch := r.Get();
			ASSERT(ch = 08BX);
			typeCount := header.types;
			NEW(types, typeCount);
			FOR i := 0 TO header.types-1 DO
				r.RawNum(size);
				r.RawNum(tdaddr);
				(* read Base *)
				r.RawNum(moduleBase); r.RawNum(entry);
				(* read Count *)
				r.RawNum(nofMethods); nofMethods := ABS (nofMethods);
				r.RawNum(nofInhMethods); r.RawNum(nofNewMethods); r.RawLInt(nofPointers);
				r.RawString(name);
				r.RawLInt(tdSize); (* ug *)
				NEW(type, name);
				IF type.name = "" THEN type.name := "[anonymous]" END;
				types[i] := type;

				KernelLog.Ln;
				KernelLog.String(" - name = "); KernelLog.String(type.name); KernelLog.Ln;
				KernelLog.String(" - size = "); KernelLog.Int(size, 0); KernelLog.Ln;
				KernelLog.String(" - tdaddr = "); KernelLog.Int(tdaddr, 0); KernelLog.Ln;
				KernelLog.String(" - moduleBase = "); KernelLog.Int(moduleBase, 0); KernelLog.Ln;
				KernelLog.String(" - entry = "); KernelLog.Int(entry, 0); KernelLog.Ln;
				KernelLog.String(" - nofMethods = "); KernelLog.Int(nofMethods, 0); KernelLog.Ln;
				KernelLog.String(" - nofInhMethods = "); KernelLog.Int(nofInhMethods, 0); KernelLog.Ln;
				KernelLog.String(" - nofNewMethods = "); KernelLog.Int(nofNewMethods, 0); KernelLog.Ln;
				KernelLog.String(" - nofPointers = "); KernelLog.Int(nofPointers, 0); KernelLog.Ln;
				KernelLog.String(" - tdSize = "); KernelLog.Int(tdSize, 0); KernelLog.Ln; (* ug *)
				KernelLog.String(" - Methods:"); KernelLog.Ln;

				(* read Methods *)
				type.procedureCount := nofNewMethods;
				NEW(type.procedures, type.procedureCount);
				FOR j := 0 TO type.procedureCount-1 DO
					r.RawNum(methNr); r.RawNum(entryNr);
					NEW(procInfo, "", module.entry[entryNr], entryNr);
					procInfo.method := TRUE;
					AddProcedure(procInfo);
					type.procedures[j] := procInfo;
				END;

				KernelLog.String(" - PtrOfs: ");
				(* read Pointers *)
				FOR j := 0 TO nofPointers-1 DO
					r.RawNum(ptrOfs);
					KernelLog.Int(ptrOfs,1); KernelLog.String(" ");
				END;
				KernelLog.Ln;
			END;
		END DecodeTypes;

		PROCEDURE DecodeExTable(r: Streams.Reader);
		VAR
			i: SIGNED32;
			tag: CHAR;
			a: SIGNED32;
		BEGIN
			NEW(module.exTable, header.exTableLen);
			FOR i := 0 TO header.exTableLen -1 DO
				r.Char(tag);
				ASSERT(tag = 0FEX);
				r.RawNum(a); module.exTable[i].pcFrom := a;
				r.RawNum(a); module.exTable[i].pcTo := a;
				r.RawNum(a); module.exTable[i].pcHandler := a;
			END;
		END DecodeExTable;

		PROCEDURE SkipPointerInProc;
		VAR ch : CHAR;
			i, j, codeoffset, beginOffset, endOffset, p, nofptrs : SIGNED32;
		BEGIN
			ch := r.Get();
			ASSERT(ch = 8FX);
			KernelLog.String(" - PointersInProc: "); KernelLog.Ln;
			NEW(gcInfo,noProcs);
			FOR i := 0 TO noProcs - 1 DO
				NEW(gcInfo[i]);
				r.RawNum(codeoffset);
				r.RawNum(beginOffset);
				r.RawNum(endOffset);
				gcInfo[i].codeOffset := codeoffset;
				gcInfo[i].beginOffset := beginOffset;
				gcInfo[i].endOffset := endOffset;
				r.RawLInt(nofptrs);
				NEW(gcInfo[i].pointers,nofptrs);
				FOR j := 0 TO nofptrs - 1 DO
					r.RawNum(p);
					gcInfo[i].pointers[j] := p;
				END
			END
		END SkipPointerInProc;

		PROCEDURE &New*(CONST fileName : ARRAY OF CHAR);
		VAR ch : CHAR; tmp : SIGNED32; j: SIGNED32; msg : ARRAY 255 OF CHAR; pos: Streams.Position;
		BEGIN
			Init;
			Strings.GetExtension (fileName, msg, ext);
			lastExt := ext;
			codeScaleCallback := GetCodeScaleCallback(ext);
			f := Files.Old(fileName);
			IF f # NIL THEN
				Files.OpenReader(r, f, 0);
				IF r.Get() = 0BBX THEN
					version := ORD(r.Get());
					IF version = 0ADH THEN version := ORD(r.Get()) END;
					IF version = 0B1H THEN (* PACO object file *)
						r.RawNum(symSize);
					ELSIF (version >= 0B2H) THEN (*fof: OC object file *)
						r.RawLInt(symSize);
					END;
					r.SkipBytes(symSize); (* skip symbol file *)
				ELSE
					KernelLog.String("Decoder: ERROR: Tag not supported or wrong file type!"); KernelLog.Ln;
					RETURN
				END;
				r.RawLInt(header.refSize);
				r.RawLInt(header.entries);
				r.RawLInt(header.commands);
				r.RawLInt(header.pointers);
				r.RawLInt(header.types);
				r.RawLInt(header.modules);
				r.RawLInt(nofVarConstLinks);
				r.RawLInt(nofLinks);
				r.RawLInt(header.dataSize);
				r.RawLInt(header.constSize);
				r.RawLInt(header.codeSize);

				IF (codeScaleCallback # NIL) THEN codeScaleCallback(header.codeSize) END;

				(* Sz: Exception handling *)
				r.RawLInt(header.exTableLen);

				(* ug: Pointers in procedures, maxPtrs, staticTdSize: *)
				r.RawLInt(noProcs);
				r.RawLInt(tmp); (* ug: skip maxPtrs *)
				r.RawLInt(tmp); (* ug: skip staticTdSize *)
				IF version > 0B3H THEN r.RawLInt(header.crc) END;
				r.RawString(name);
				(* skip to code block *)
				DecodeEntries;
				SkipCommands;
				SkipPointers;
				SkipImports;
				SkipVarConstLinks;
				SkipLinks;
				SkipConsts;
				SkipExports;
				ch := r.Get();
				pos := r.Pos();
				ASSERT(ch = 89X);

				(* code section *)
				NEW(module.code, header.codeSize);
				FOR j := 0 TO header.codeSize-1 DO
					module.code[j] := r.Get()
				END;
				SkipUse;
				DecodeTypes;

				(* Sz: read exception handling table *)
				ch := r.Get();
				ASSERT(ch = 8EX);
				DecodeExTable(r);

				SkipPointerInProc; (* ug *)

				(* read ref header *)
				ch := r.Get();
				ASSERT(ch = OFFHdrRef);
				DecodeRefs(r);
			ELSE
				msg := "Object file '"; Strings.Append(msg, fileName); Strings.Append(msg, "' could not be found.");
				KernelLog.String(msg);KernelLog.Ln;
			END;
		END New;

	END ModuleInfoObjectFile;

	Extension = ARRAY 4 OF CHAR;

	DecoderType = OBJECT
		VAR
			ext : Extension;
			decoderFactory : DecoderFactory;
			codeScaleCallback : CodeScaleCallback;

		PROCEDURE &New (CONST ext : Extension; decoderFactory : DecoderFactory; codeScaleCallback : CodeScaleCallback);
		BEGIN
			SELF.ext := ext; SELF.decoderFactory := decoderFactory; SELF.codeScaleCallback := codeScaleCallback
		END New;
	END DecoderType;

VAR
	decoderTypes : ARRAY maxDecoders OF DecoderType;
	nofDecoders : SIGNED32;
	lastExt : Extension;



	PROCEDURE IntToHex*(h, width: SIGNED32; VAR s: ARRAY OF CHAR);
	VAR c: CHAR;
	BEGIN
		IF (width <= 0) THEN width := 8 END;
		ASSERT(LEN(s) > width);
		s[width] := 0X;
		DEC(width);
		WHILE (width >= 0) DO
			c := CHR(h MOD 10H + ORD("0"));
			IF (c > "9") THEN c := CHR((h MOD 10H - 10) + ORD("A")) END;
			s[width] := c; h := h DIV 10H; DEC(width)
		END
	END IntToHex;

	PROCEDURE GetDecoderType*(CONST ext : Extension) : DecoderType;
	VAR i : SIGNED32;
	BEGIN
		IF nofDecoders < 1 THEN RETURN NIL END;
		IF ext = "" THEN RETURN decoderTypes[0] END;
		FOR i := 0 TO nofDecoders-1 DO
			IF decoderTypes[i].ext = ext THEN
				RETURN decoderTypes[i]
			END
		END;
		RETURN NIL
	END GetDecoderType;

	PROCEDURE GetDecoder (CONST ext : Extension; reader : Streams.Reader) : Decoder;
	VAR dec : DecoderType;
	BEGIN
		dec := GetDecoderType(ext);
		IF dec # NIL THEN RETURN dec.decoderFactory(reader)
		ELSE RETURN NIL
		END
	END GetDecoder;

	PROCEDURE RegisterDecoder* (CONST ext : Extension; decFactory : DecoderFactory; csclCallback : CodeScaleCallback);
	VAR dec : DecoderType;
	BEGIN
		ASSERT(nofDecoders < maxDecoders);
		dec := GetDecoderType(ext);
		IF dec = NIL THEN
			NEW(decoderTypes[nofDecoders], ext, decFactory, csclCallback);
			INC(nofDecoders)
		END
	END RegisterDecoder;

	PROCEDURE GetCodeScaleCallback (CONST ext : Extension) : CodeScaleCallback;
	VAR dec : DecoderType;
	BEGIN
		dec := GetDecoderType(ext);
		IF dec # NIL THEN RETURN dec.codeScaleCallback
		ELSE RETURN NIL
		END
	END GetCodeScaleCallback;

	PROCEDURE Initialize (CONST decoder: ARRAY OF CHAR);
	VAR initializer: PROCEDURE;
	BEGIN
		GETPROCEDURE (decoder, "Init", initializer);
		IF initializer # NIL THEN initializer END;
	END Initialize;

PROCEDURE Test*;
VAR
	mi:ModuleInfo;
	mdi:ModuleInfoObjectFile;
	tw : TextUtilities.TextWriter;
	text : Texts.Text;
BEGIN
	NEW(mdi,"TestDbg.Obw");
	NEW(text);
	NEW(tw,text);
	mi:=mdi;
	mi.OutlineProcedure (mdi.GetProcedureByIndex(1),tw);
END Test;

BEGIN
	nofDecoders := 0;
	Initialize ("I386Decoder2");
END BtDecoder.Test~

BtDecoder.Init~
System.FreeDownTo I386Decoder2 BtDecoder ~

WMDebugger.Open WMDebugger/BtDecoder.Mod WMDebugger/I386Decoder2.Mod~

