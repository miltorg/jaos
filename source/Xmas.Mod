 (** AUTHOR "BohdanT"; PURPOSE "Just for fun"; based on http://js1k.com/2010-xmas/demo/856*)
MODULE Xmas;

IMPORT
	Modules, WMComponents, WM := WMWindowManager, WMMessages, WMStandardComponents,
	Strings,WMRectangles, WMGraphics, Raster,M:=Math,Random, KernelLog, Kernel, Commands;

CONST
	size = 32; W=446;U=16;
	posX = 100; 
	posY =100;

sc = (*sincos table for draw circle*)
[[1.00000000,0.00000000],
[0.99518473,0.09801714],
[0.98078528,0.19509033],
[0.95694034,0.29028468],
[0.92387953,0.38268344],
[0.88192125,0.47139676],
[0.83146961,0.55557023],
[0.77301043,0.63439332]];

TYPE
	Elem = ARRAY 4 OF FLOAT32;

	KillerMsg = OBJECT
	END KillerMsg;

	XmasTree*=OBJECT(WM.BufferWindow)
	VAR
		alive : BOOLEAN;
			timer : Kernel.Timer;
			c:WMGraphics.BufferCanvas;
			sprites:ARRAY 14 OF  Raster.Image;
			J:ARRAY 4279 OF Elem;
			count:SIGNED32;
			A,D:FLOAT32;
			RND:Random.Generator;
			fnt:WMGraphics.Font;
			img : Raster.Image;

	PROCEDURE &New*;
	BEGIN
		bounds :=WMGraphics.MakeRectangle(0, 0, W, W);
		Init(W, W, TRUE);

		useAlpha := TRUE;
		isVisible := TRUE;

		fnt := WMGraphics.GetFont("Vera", 40, {0});
		alive:=TRUE;
		NEW(RND);
		NEW(timer);
		CreateSprites();
		CreateArray();
		manager := WM.GetDefaultManager();
		manager.Add(posX, posY, SELF, {3});

	END New;
	PROCEDURE CreateBall*(size:SIGNED32;VAR image: Raster.Image;f:SIGNED32);
	VAR i,r,I : SIGNED32;
		p : ARRAY 64 OF WMGraphics.Point2d;
		X,Y,x,y:FLOAT32;
		color:SIGNED32;
		c:WMGraphics.BufferCanvas;
	BEGIN
		NEW(image);
		Raster.Create(image, size, size, Raster.BGRA8888);
		NEW(c,image);
		c.Fill(WMRectangles.MakeRect(0, 0, size, size),0,WMGraphics.ModeCopy);
		I:=96;
		FOR r:=0 TO 7  DO
		X:=U-r/3;Y:=24-r/2;
			IF f=0 THEN 
				color:=WMGraphics.RGBAToColor(205,205,215,38H);
			ELSIF f=1 THEN
				color:=WMGraphics.RGBAToColor((147+I),128+I,I,80H)
			ELSE 
				color:=WMGraphics.RGBAToColor((147+I),0,I,80H);
			END;
(*			IF f=0 THEN 
				color:=WMGraphics.RGBAToColor(205,215,205,38H);
			ELSIF f=1 THEN
				color:=WMGraphics.RGBAToColor((147+I),I,128+I,80H)
			ELSE 
				color:=WMGraphics.RGBAToColor((147+I),0,I,80H);
			END;*)
			I:=r*U;
			
			FOR i := 0 TO 7 DO 
				IF f=0 THEN
					x := (4-(r)/2) *sc[i,0];y := (4-(r)/2) * sc[i,1];
				ELSE
					x := (8-r) *sc[i,0];y := (8-r) * sc[i,1];
				END;
				p[00+i].x := ENTIER(X + x + 0.5); p[00+i].y := ENTIER(Y + y + 0.5);
				p[15-i].x := ENTIER(X + y + 0.5 );p[15-i].y := ENTIER(Y + x + 0.5);
				p[16+i].x := ENTIER(X - y + 0.5 );p[16+i].y := ENTIER(Y + x + 0.5);
				p[31-i].x := ENTIER(X - x + 0.5); p[31-i].y := ENTIER(Y + y + 0.5);
		(**************)
				p[32+i].x := ENTIER(X - x + 0.5);p[32+i].y := ENTIER(Y -  y + 0.5);
				p[47-i].x := ENTIER(X - y + 0.5) ;p[47-i].y := ENTIER(Y - x + 0.5);
				p[i+48].x := ENTIER(X + y + 0.5) ;p[i+48].y := ENTIER(Y - x + 0.5) ;
				p[63-i].x := ENTIER(X + x + 0.5) ;p[63-i].y := ENTIER(Y - y + 0.5);
			END;
			c.FillPolygonFlat(p, 64, color, WMGraphics.ModeCopy);
		END;
	END CreateBall;

	PROCEDURE CreateSprites;
	VAR
		i,k:SIGNED16;
		x,y,D,B,L:FLOAT32;
		R:SIGNED32;
	BEGIN
		FOR k:=0 TO 10 DO 
			NEW(sprites[k]);
			Raster.Create(sprites[k], size, size, Raster.BGRA8888);
			NEW(c,sprites[k]);
			c.Fill(WMRectangles.MakeRect(0, 0, size, size),0,WMGraphics.ModeCopy);
			i:=0;
			WHILE i<400 DO
				INC(i);
				x:=M.sin(i);
				y:=RND.Dice(360)/180-1;
				D:=x*x+y*y;
				B:=M.sqrt(D-x/0.9-1.5*y+1);
				L:=k/9;
				R:=ENTIER(67*(B+1)*(L+0.8)) DIV 2;
				IF D<1 THEN
  					c.Line(ENTIER(U+x*8),ENTIER(U+y*8),ENTIER(U+x*U),ENTIER(U+y*U),WMGraphics.RGBAToColor(R,ENTIER(R+B*L),40,25),WMGraphics.ModeSrcOverDst);
				END;
			END
		END;
		CreateBall(32,sprites[11],1);
		CreateBall(32,sprites[12],2);
		CreateBall(32,sprites[13],0);

	END CreateSprites;

	PROCEDURE CreateArray;
	VAR
	k:SIGNED32;
	R,i:SIGNED32;
	H,P,x,y,z,jj:FLOAT32;
	rando:FLOAT32;
	BEGIN
		k:=0;count:=0;
		WHILE(k<200)DO
			x:=0;z:=0;i:=0;jj:=0;
			H:=k+M.sqrt(k)*25;
			y:=H;
			R:=RND.Dice(W);
			WHILE(jj<H)DO
				P:=3;
				x:=x+M.sin(R)*P+RND.Dice(6)-3;
				z:=z+M.sin(R-11)*P+RND.Dice(6)-3;
				y:=y+RND.Dice(U)-8;
				IF ((jj+U)>H) & (RND.Dice(1000)>800) THEN
					J[count,0]:=x;
					J[count,1]:=y;
					J[count,2]:=z;
					rando:=RND.Dice(400)/100;
					J[count,3]:=ENTIER((jj/H*20+rando)/2);
				ELSE
					J[count,0]:=x;
					J[count,1]:=y;
					J[count,2]:=z;
					J[count,3]:=ENTIER((jj/H*20)/2);
				END;
				INC(count);
				jj:=jj+U;
			END;
			INC(k);
		END;
			KernelLog.String("CreateArray= "); KernelLog.Int(count, 0); KernelLog.Ln;
	END CreateArray;

		PROCEDURE GetOberon*; END GetOberon;
		
		PROCEDURE comp(CONST m, l: Elem): BOOLEAN;
		VAR
		 tmp:FLOAT32;
		BEGIN
			tmp:=(m[2]-l[2])*A+(l[0]-m[0])*M.sin(D);
			RETURN tmp>0;
		END comp;
		PROCEDURE QuickSort(lo, hi: SIGNED32);

		VAR i, j: SIGNED32; x, t: Elem;
		BEGIN
(*		KernelLog.String("QuickSort lo= "); KernelLog.Int(lo, 0); 
		KernelLog.String(" hi= "); KernelLog.Int(hi, 0); KernelLog.Ln;*)
			i := lo; j := hi;
			x := J[(lo+hi) DIV 2];
			
			WHILE (i <= j) DO
				WHILE (comp(J[i], x)) DO INC(i) END;
				WHILE (comp(x, J[j])) DO DEC(j) END;
				IF (i <= j) THEN
					t := J[i]; J[i] := J[j]; J[j] := t;
					INC(i); DEC(j)
				END
			END;
			
			IF (lo < j) THEN QuickSort(lo, j) END;
			IF (i < hi) THEN QuickSort(i, hi) END
		END QuickSort;

		

		PROCEDURE Draw(canvas : WMGraphics.Canvas; w, h, q : SIGNED32);
		VAR
			i:SIGNED32;
			L:Elem;
		BEGIN
			canvas.SetColor(WMGraphics.Green);
			
(*			canvas.Fill(bounds,0,WMGraphics.ModeCopy);*)
			canvas.SetFont(fnt);

			A:=M.sin(D-11);
			QuickSort(0, count-1);
			i:=0;
			WHILE(i<count) DO
				L:=J[i];
				canvas.DrawImage(ENTIER(207+L[0]*A+L[2]*M.sin(D)),ENTIER(L[1]/2),sprites[ENTIER(L[3]+1)],WMGraphics.ModeSrcOverDst);
				INC(i);
				IF i MOD 7=0 THEN
					canvas.DrawImage(
					
					ENTIER( 157*i*i+M.sin(D*5+i*i)*5) MOD 460,
					ENTIER(113*i+(D*i)/60) MOD ENTIER(290+i/99),
					sprites[13],WMGraphics.ModeSrcOverDst);
				END;
			END;
		(*	canvas.DrawString(U,W-U,"Happy New Year!!!");*)

			D:=D+0.02;

		END Draw;

		PROCEDURE Close;
		BEGIN alive := FALSE
		END Close;

		PROCEDURE Handle(VAR x : WMMessages.Message);
		BEGIN
			IF (x.msgType = WMMessages.MsgExt) & (x.ext # NIL) & (x.ext IS KillerMsg) THEN Close
			ELSE Handle^(x)
			END
		END Handle;


	BEGIN {ACTIVE}
		alive := TRUE;
		WHILE alive DO timer.Sleep(500); Invalidate(WMRectangles.MakeRect(0, 0, W, W)); END;
	FINALLY
		manager.Remove(SELF);
		DecCount;
	END XmasTree;
	
VAR
	nofWindows : SIGNED32;

(** Standard multiple windows controlled by number, non-restorable. *)
PROCEDURE Open* ;
VAR winstance :XmasTree;
BEGIN
	NEW (winstance);
END Open;

PROCEDURE IncCount;	(* E *)
BEGIN {EXCLUSIVE}
	INC (nofWindows);
END IncCount;		

PROCEDURE DecCount;	(* F *)
BEGIN {EXCLUSIVE}
	DEC (nofWindows);
END DecCount;	

PROCEDURE Cleanup;	(* G *)
VAR die : KillerMsg;
	 msg : WMMessages.Message;
	 m : WM.WindowManager;
BEGIN {EXCLUSIVE}
	NEW (die);
	msg.ext := die;
	msg.msgType := WMMessages.MsgExt;
	m := WM.GetDefaultManager ();
	m.Broadcast (msg);
	AWAIT (nofWindows = 0)
END Cleanup;

BEGIN
	Modules.InstallTermHandler (Cleanup);

	(* Additional BEGIN processing. *)
END Xmas.

Xmas.Open ~
System.Free Xmas ~
WMDebugger.Open WMDebugger/Xmas.Mod~


