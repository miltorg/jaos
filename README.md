ЯОС (рабочее название)
======================

Яос - форк операционной системы A2 (также известной как Bluebottle и Active Oberon).
Основная цель - кардинальное внедрение русского языка во всю систему, включая перевод
исходных текстов на русский язык. 

# Отличия от A2 на 2020-05-04

* переход к определению в редакторе TFPET
* описана сборка и запуск под QEMU, Ubuntu 18.04, Windows 
* пошаговый отладчик (работает изредка)
* почти все шрифты поддерживают кириллицу
* кириллица работает в текстовом режиме, в буфере обмена
* починено масштабирование текстов в формате BBT
* доступен ввод букв кириллицы без дополнительных усилий по конфигурированию
* добавлены некоторые сочетания клавиш
* частично переведено на русский язык главное меню

Часть этих изменений относятся к коду, часть - к документации, часть - к конфигурации. 
Поскольку идёт обмен кодом и идеями с командой A2, список может уменьшаться или увеличиваться. 

# видео

* [A2 под Win, Lin и на виртуальном железе](https://www.youtube.com/watch?v=9e0hJjz64Tw)
* [Проект "программирование по-русски", часть 4. Яр и A2](https://www.youtube.com/watch?v=HMiUL7rYg4w)


# Документация

На данный момент документация несколько разбросана, см. 

* http://вики-ч115.программирование-по-русски.рф
* [Документация в репозитории](док/оглавление.md)
* [Форум](http://вече.программирование-по-русски.рф/viewforum.php?f=5)
